//
//  POIAnnotationView.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 18/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import MapKit

class POIAnnotationView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let annotation = newValue as? POIAnnotation else { return }
            switch annotation.type {
            case .some(let type):
                self.glyphText = annotation.glyphText
                switch type {
                case .me:
                    self.markerTintColor = .red
                    break
                case .trader:
                    self.markerTintColor = .blue
                    break
                case .destination:
                    self.markerTintColor = .orange
                    break
                }
            default:
                self.glyphText = "?"
                self.markerTintColor = .black
            }
        }
    }
}
