//
//  NodeProtocol.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 01/02/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import ARKit
import CoreLocation

protocol Node: class {
    var type: NodeType { get }
    var step: Step { get set }
    var anchor: ARAnchor? { get set }
    
    func updateNode(animationDuration: Double, originCoordinate: CLLocationCoordinate2D, completion: (()->())?)
    func updateNode(animationDuration: Double, originCoordinate: CLLocationCoordinate2D, coordinate: CLLocationCoordinate2D, completion: (()->())?)
    func addNode(named: String)
}

extension Node where Self: SCNNode {
    func updateNode(animationDuration: Double, originCoordinate: CLLocationCoordinate2D, completion: (()->())?) {
        DispatchQueue.global(qos: .userInteractive).async {
            let translation = originCoordinate.transformMatrix(coordinate: self.step.coordinate)
            let anchor: ARAnchor = ARAnchor(transform: translation)

            SCNTransaction.begin()
            SCNTransaction.animationDuration = animationDuration
            self.anchor = anchor
            SCNTransaction.commit()
        }
        completion?()
    }
    
    func updateNode(animationDuration: Double, originCoordinate: CLLocationCoordinate2D, coordinate: CLLocationCoordinate2D, completion: (()->())?) {
        DispatchQueue.global(qos: .userInteractive).async {
            let translation = originCoordinate.transformMatrix(coordinate: coordinate)
            let anchor: ARAnchor = ARAnchor(transform: translation)

            SCNTransaction.begin()
            SCNTransaction.animationDuration = animationDuration
            self.anchor = anchor
            SCNTransaction.commit()
        }
        completion?()
    }
    
    func removeAllChildNodes() {
        self.enumerateChildNodes { (node, stop) in node.removeFromParentNode() }
    }
}
