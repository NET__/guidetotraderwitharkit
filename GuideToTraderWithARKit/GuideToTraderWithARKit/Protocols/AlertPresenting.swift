//
//  AlertPresenting.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 28/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import UIKit

protocol AlertPresenting {
    func presentMessage(title: String, message: String)
}

extension AlertPresenting where Self: UIViewController {
    func presentMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        present(alertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            alertController.dismiss(animated: true, completion: nil)
        }
    }
}
