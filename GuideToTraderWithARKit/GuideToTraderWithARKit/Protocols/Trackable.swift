//
//  TrackingAccordingToTypeDelegate.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 23/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import Foundation

protocol Trackable: class {
    func trackingMyCoordinate(for currentStep: Step)
    func trackingTraderCoordinate(for currentStep: Step)
}
