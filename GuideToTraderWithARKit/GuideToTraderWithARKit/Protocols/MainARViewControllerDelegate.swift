//
//  MainARViewControllerDelegate.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 23/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

protocol MainARViewControllerDelegate: class {
    func myStepChanged(to: Step)
    func traderStepChanged(to: Step)
}
