//
//  MapViewController.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 14/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreMotion

class MapViewController: UIViewController, UIGestureRecognizerDelegate, AlertPresenting {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var refreshContainerView: UIView!
    
    private var userAnnotation: POIAnnotation?
    private var traderAnnotation: POIAnnotation?
    private var destinationAnnotation: POIAnnotation?
    private var selectAnnotation: POIAnnotation?
    private var userSteps: [Step] = []
    private var traderSteps: [Step] = []
    
    private let speakerService: SpeakerService = SpeakerService()
    
    private var onceAlertPresenting: Bool = false
    
    private final let identifier: String = "POIAnnotationView"
    private final let period: Double = 2.0
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.excute(tasks: [self.setUpTestCase, self.setUpMapView, self.setUpSegmentedControl, self.setUpRefreshContainerView])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let userAnnotation = self.userAnnotation {
            self.mapView.removeAnnotation(userAnnotation)
            self.mapView.addAnnotation(userAnnotation)
        }
        
        if let traderAnnotation = self.traderAnnotation {
            self.mapView.removeAnnotation(traderAnnotation)
            self.mapView.addAnnotation(traderAnnotation)
        }

        self.userAnnotation?.fakeLocationService?.isPause = false
        self.traderAnnotation?.fakeLocationService?.isPause = false
        self.determineARMode()
    }
    
    // MARK: Set Up Instance
    
    private func excute(tasks: [()-> Void]) {
        for task in tasks {
            task()
        }
    }
    
    private func setUpTestCase() {
        self.userAnnotation = POIAnnotation(coordinate: CLLocationCoordinate2D(latitude: 37.800009, longitude: 127.485881), title: "나", glyphText: "M", userType: .me)
        self.destinationAnnotation = POIAnnotation(coordinate: CLLocationCoordinate2D(latitude: 37.803001, longitude: 127.491343), title: "도착지", glyphText: "D", userType: .destination)
        self.traderAnnotation = POIAnnotation(coordinate: CLLocationCoordinate2D(latitude: 37.803866, longitude: 127.494315), title: "거래자", glyphText: "T", userType: .trader)

        DispatchQueue.main.async {
            self.mapView.addAnnotation(self.userAnnotation!)
            self.mapView.addAnnotation(self.traderAnnotation!)
            self.mapView.addAnnotation(self.destinationAnnotation!)
        }
        self.userToDestination()
        self.traderToDestination()
    }
    
    private func setUpMapView() {
        self.mapView.register(POIAnnotationView.self, forAnnotationViewWithReuseIdentifier: self.identifier)
        self.mapView.delegate = self
        
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 0.5
        longPressGestureRecognizer.delegate = self
        self.mapView.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    private func setUpRefreshContainerView() {
        self.refreshContainerView.layer.cornerRadius = 5
    }
    
    private func setUpSegmentedControl() {
        self.segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
    }
    
    // MARK: Draw Overlay & Set Up POIAnnotation
    
    private func userToDestination() {
        if let start = self.userAnnotation?.coordinate, let destination = self.destinationAnnotation?.coordinate {
            start.getDirections(destinationCoordinate: destination, request: MKDirections.Request()) { [weak self]  steps, route in
                guard let self = self else { return }
                self.userSteps = steps
                
                DispatchQueue.main.async {
                    self.mapView.addOverlay(route.polyline, level: MKOverlayLevel.aboveLabels)
                    let rect = route.polyline.boundingMapRect
                    
                    let coordinateRegion = MKCoordinateRegion(rect)
                    self.mapView.setRegion(coordinateRegion, animated: true)
                }
                print("[info] .... calculate user to destination direction complete")

                self.userAnnotation?.trackable = self
                self.userAnnotation?.fakeLocationService = FakeLocationService(steps: steps, periodBetween: self.period)
                self.userAnnotation?.fakeLocationService?.delegate = self.userAnnotation
                self.userAnnotation?.fakeLocationService?.startTimer()
            }
        }
    }
    
    private func traderToDestination() {
        if let trader = self.traderAnnotation?.coordinate, let destination = self.destinationAnnotation?.coordinate {
            trader.getDirections(destinationCoordinate: destination, request: MKDirections.Request()) { [weak self] steps, route in
                guard let self = self else { return }
                self.traderSteps = steps

                DispatchQueue.main.async {
                    self.mapView.addOverlay(route.polyline, level: MKOverlayLevel.aboveLabels)
                }
                print("[info] .... calculate trader to destination direction complete")

                self.traderAnnotation?.trackable = self
                self.traderAnnotation?.fakeLocationService = FakeLocationService(steps: steps, periodBetween: self.period)
                self.traderAnnotation?.fakeLocationService?.delegate = self.traderAnnotation
                self.traderAnnotation?.fakeLocationService?.startTimer()
            }
        }
    }
    
    @objc private func segmentedControlValueChanged(segment: UISegmentedControl) {
        self.mapView.removeAnnotation(self.selectAnnotation!)
        
        if let annotation = self.selectAnnotation {
            switch segmentedControl.selectedSegmentIndex {
            case 0:
                if self.userAnnotation?.fakeLocationService != nil {
                    break
                }
                
                if let annotation = self.userAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.removeAnnotation(annotation)
                    }
                }
                
                self.userAnnotation = POIAnnotation(coordinate: annotation.coordinate, title: "나", glyphText: "M", userType: .me)

                if let annotation = self.userAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.addAnnotation(annotation)
                    }
                }
                
                if let _ = self.destinationAnnotation {
                    self.userToDestination()
                }
                break
            case 1:
                if self.traderAnnotation?.fakeLocationService != nil {
                    break
                }

                if let annotation = self.traderAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.removeAnnotation(annotation)
                    }
                }
                
                self.traderAnnotation = POIAnnotation(coordinate: annotation.coordinate, title: "거래자", glyphText: "T", userType: .trader)
                
                if let annotation = self.traderAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.addAnnotation(annotation)
                    }
                }
                
                if let _ = self.destinationAnnotation {
                    self.traderToDestination()
                }
                break
            case 2:
                if self.userAnnotation?.fakeLocationService != nil || self.traderAnnotation?.fakeLocationService != nil {
                    break
                }
                
                if let annotation = self.destinationAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.removeAnnotation(annotation)
                    }
                }
                
                self.destinationAnnotation = POIAnnotation(coordinate: annotation.coordinate, title: "도착지",  glyphText: "D", userType: .destination)
                
                if let destinationAnnotation = self.destinationAnnotation {
                    DispatchQueue.main.async {
                        self.mapView.addAnnotation(destinationAnnotation)
                    }
                    
                    if let _ = self.userAnnotation {
                        self.determineARMode()
                        self.userToDestination()
                    }

                    if let _ = self.traderAnnotation {
                        self.traderToDestination()
                    }
                }
                break
            default:
                break
            }
        }
        
        self.selectAnnotation = nil
        self.segmentedControl.selectedSegmentIndex = -1
        DispatchQueue.main.async {
            self.segmentedControl.isHidden = true
            self.segmentedControlHeightConstraint.constant = 0
        }
    }

    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        let coordinate = self.mapView.convert(gestureRecognizer.location(in: self.mapView), toCoordinateFrom: self.mapView)
        
        if let selectAnnotation = self.selectAnnotation {
            DispatchQueue.main.async {
                self.mapView.removeAnnotation(selectAnnotation)
            }
        }
        
        self.selectAnnotation = POIAnnotation(coordinate: coordinate)
        DispatchQueue.main.async {
            self.mapView.addAnnotation(self.selectAnnotation!)
            self.segmentedControl.isHidden = false
            self.segmentedControlHeightConstraint.constant = 40
        }
    }
    
    @IBAction func refreshButtonClicked(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.mapView.removeOverlays(self.mapView.overlays)
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        self.userAnnotation = nil
        self.destinationAnnotation = nil
        self.traderAnnotation = nil
        self.selectAnnotation = nil
        
        self.onceAlertPresenting = false
        CMMotionManager.shared.stopDeviceMotionUpdates()
        
        print("[info] .... refresh properties")
    }
    
    // MARK: Check Distance & Manage Motion
    
    private func determineARMode() {
        guard let userAnnotation = self.userAnnotation, let destinationAnnotation = self.destinationAnnotation else { return }
        if userAnnotation.coordinate.distance(from: destinationAnnotation.coordinate) <= 500 {
            if self.onceAlertPresenting == false {
                print("[info] .... Motion Manager Enable")
                self.presentMessage(title: "AR 모드 활성화", message: "목적지가 500m 이내에 있습니다.")
                self.onceAlertPresenting = true
            } else {
                self.motionManagerStart()
            }
        } else {
            CMMotionManager.shared.stopDeviceMotionUpdates()
        }
    }
    
    private func motionManagerStart() {
        guard !CMMotionManager.shared.isDeviceMotionActive else { return }
        print("[info] .... Motion Manager Start")

        CMMotionManager.shared.startQueuedUpdates() { [weak self] angle in
            guard let self = self else { return false }
            if angle > 70 {
                self.userAnnotation?.fakeLocationService?.isPause = true
                self.traderAnnotation?.fakeLocationService?.isPause = true
                let arVC = MainARViewController(userCoordinate: self.userAnnotation!.coordinate, userSteps: self.userSteps, traderCoordinate: self.traderAnnotation?.coordinate, traderSteps: self.traderSteps, destinationCoordinate: self.destinationAnnotation!.coordinate)
                arVC.delegate = self
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(arVC, animated: true)
                }
                return true
            }
            return false
        }
    }
}

// MARK: MapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .green
        renderer.lineWidth = 5.0
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? POIAnnotation else { return nil }

        var view: POIAnnotationView

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: self.identifier)
            as? POIAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = POIAnnotationView(annotation: annotation, reuseIdentifier: self.identifier)
        }
    
        return view
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        self.segmentedControl.isHidden = true
        self.segmentedControlHeightConstraint.constant = 0
        if let selectAnnotation = self.selectAnnotation {
            self.mapView.removeAnnotation(selectAnnotation)
        }
    }
}

// MARK: Trackable

extension MapViewController: Trackable {
    func trackingMyCoordinate(for currentStep: Step) {
        guard let userAnnotation = self.userAnnotation else { return }
        print("[info] .... My location changed : \(userAnnotation.coordinate.latitude), \(userAnnotation.coordinate.longitude) -> \(currentStep.coordinate.latitude), \(currentStep.coordinate.longitude)")

        if let instructions = currentStep.instructions, let distance = currentStep.distance {
            self.speakerService.speak(distance: Int(distance), directions: instructions)
        }
        
        self.determineARMode()
        DispatchQueue.main.async {
            self.mapView.removeAnnotation(userAnnotation)
            userAnnotation.coordinate = currentStep.coordinate
            self.mapView.addAnnotation(userAnnotation)
        }
    }
    
    func trackingTraderCoordinate(for currentStep: Step) {
        guard let traderAnnotation = self.traderAnnotation else { return }
        print("[info] .... Trader location changed : \(traderAnnotation.coordinate.latitude), \(traderAnnotation.coordinate.longitude) -> \(currentStep.coordinate.latitude), \(currentStep.coordinate.longitude)")

        DispatchQueue.main.async {
            self.mapView.removeAnnotation(traderAnnotation)
            traderAnnotation.coordinate = currentStep.coordinate
            self.mapView.addAnnotation(traderAnnotation)
        }
    }
}

// MARK: MainARViewControllerDelegate

extension MapViewController: MainARViewControllerDelegate {
    func myStepChanged(to: Step) {
        print("[info] .... ARViewController Pass My Location To MapViewController")

        self.userAnnotation?.coordinate = to.coordinate
        self.userAnnotation?.fakeLocationService?.setCounter(with: to)
    }
    
    func traderStepChanged(to: Step) {
        print("[info] .... ARViewController Pass Trader Location To MapViewController")

        self.traderAnnotation?.coordinate = to.coordinate
        self.traderAnnotation?.fakeLocationService?.setCounter(with: to)
    }
}
