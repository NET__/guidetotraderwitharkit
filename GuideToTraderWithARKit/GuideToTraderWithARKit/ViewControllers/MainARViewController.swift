//
//  MainARViewController.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 10/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import UIKit
import ARKit
import CoreLocation
import CoreMotion

class MainARViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var trackingStateLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var informationButton: UIButton!
    @IBOutlet weak var informationTextView: UITextView!
    
    weak var delegate: MainARViewControllerDelegate?

    private let speakerService = SpeakerService()
    private let userSteps: [Step]
    private let traderSteps: [Step]?
    private let user: ObjectNode
    private let destination: ObjectNode
    
    private var trader: ObjectNode?
    private var userToDestinationManager: WaypointNodeManager
    private var traderToDestinationManager: WaypointNodeManager?
    private final let duration: Double = 2
    private final let radius: Double = 200
    
    init(userCoordinate: CLLocationCoordinate2D, userSteps: [Step], traderCoordinate: CLLocationCoordinate2D? = nil, traderSteps: [Step]? = nil, destinationCoordinate: CLLocationCoordinate2D) {

        self.userSteps = userSteps
        self.traderSteps = traderSteps
        
        self.user = ObjectNode(title: "나", step: Step(coordinate: userCoordinate), type: .me)
        self.userToDestinationManager = WaypointNodeManager(steps: userSteps, radius: self.radius, type: .fromMe)
        
        self.destination = ObjectNode(title: "목적지", step: Step(coordinate: destinationCoordinate), type: .destination)

        if let traderCoordinate = traderCoordinate, let traderSteps = traderSteps {
            self.trader = ObjectNode(title: "거래자", step: Step(coordinate: traderCoordinate), type: .trader)
            self.traderToDestinationManager = WaypointNodeManager(steps: traderSteps, radius: self.radius, type: .fromTrader)
        }
        
        super.init(nibName: "MainARViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.excute(tasks: [self.motionManagerStart, self.setUpScene, self.setUpFakeLocationServices, self.setUpInformationButton])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        self.sceneView.session.delegate = self
        self.sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sceneView.session.pause()
    }
    
    // MARK: Set Up Instances
    
    private func excute(tasks: [()-> Void]) {
        for task in tasks {
            task()
        }
    }
    
    private func motionManagerStart() {
        CMMotionManager.shared.startQueuedUpdates() { [weak self] angle in
            guard let self = self else { return false }
            if angle <= 30 {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
                return true
            }
            return false
        }
    }

    private func setUpFakeLocationServices() {
        self.user.trackable = self

        let userInitialCounter = self.userSteps.firstIndex(where: {
            $0.coordinate == self.user.step.coordinate
        })
        self.user.fakeLocationService = FakeLocationService(steps: self.userSteps, periodBetween: self.duration, initialCounter: userInitialCounter)
        self.user.fakeLocationService?.delegate = self.user
        
        
        guard let trader = self.trader, let traderSteps = self.traderSteps else { return }
        self.trader?.trackable = self
        
        let traderInitialCounter = traderSteps.firstIndex(where: {
            $0.coordinate == trader.step.coordinate
        })

        trader.fakeLocationService = FakeLocationService(steps: traderSteps, periodBetween: self.duration, initialCounter: traderInitialCounter)
        trader.fakeLocationService?.delegate = self.trader
    }
    
    private func setUpScene() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sceneViewTapped))
        tapGestureRecognizer.delegate = self
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)
        self.sceneView.scene = SCNScene()

        self.destination.addNode(named: "art.scnassets/flag.scn")
        self.sceneView.scene.rootNode.addChildNode(self.destination)
        self.destination.updateNode(animationDuration: 0, originCoordinate: self.user.step.coordinate, completion: nil)
        
        self.userToDestinationManager.renderingWaypointNodes(rootNode: self.sceneView.scene.rootNode, originCoordinate: self.user.step.coordinate, duration: 0)

        if let trader = self.trader {
            trader.addNode(named: "art.scnassets/sunLightPin.scn")
            self.sceneView.scene.rootNode.addChildNode(trader)
            trader.updateNode(animationDuration: 0, originCoordinate: self.user.step.coordinate, completion: nil)
            
            self.traderToDestinationManager?.renderingWaypointNodes(rootNode: self.sceneView.scene.rootNode, originCoordinate: self.user.step.coordinate, duration: 0)
        }
    }
    
    private func setUpInformationButton() {
        self.informationButton.addTarget(self, action: #selector(informationButtonDidClicked), for: .touchDown)
    }
    
    @objc func sceneViewTapped(gestureRecognizer: UITapGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            let location = gestureRecognizer.location(in: self.sceneView)
            let hits = self.sceneView.hitTest(location, options: [SCNHitTestOption.ignoreChildNodes : ObjectNode.self])
            if let tappedNode = hits.first?.node as? ObjectNode {
                tappedNode.isSelected == false ?
                    tappedNode.addInformation() : tappedNode.removeInformation()
                tappedNode.isSelected = !tappedNode.isSelected
            }
        }
    }
    
    @objc func informationButtonDidClicked() {
        self.informationTextView.isHidden = !self.informationTextView.isHidden
        if !self.informationTextView.isHidden {
            self.setUpLogInformationTextView()
        }
    }
    
    func setUpLogInformationTextView() {
        let startLatitude = self.user.step.coordinate.latitude.roundUp(at: 6)
        let startLongitude = self.user.step.coordinate.longitude.roundUp(at: 6)
        let userToDestinationDistance = self.user.step.coordinate.distance(from: self.destination.step.coordinate).roundUp(at: 4)
        
        self.informationTextView.text = "[my location]\n" +
            "lat \(startLatitude), lon : \(startLongitude) \n" +
        "to destination : \(userToDestinationDistance)m \n\n"
        
        guard let traderCoordinate = self.trader?.step.coordinate else { return }
        let traderLatitude = traderCoordinate.latitude.roundUp(at: 6)
        let traderLongitude = traderCoordinate.longitude.roundUp(at: 6)
        let traderToDestinationDistance = traderCoordinate.distance(from: self.destination.step.coordinate).roundUp(at: 4)
        
        let traderInformationString = "[trader location]\n" + "lat \(traderLatitude), lon : \(traderLongitude) \n" + "to destination : \(traderToDestinationDistance)m \n\n"
        self.informationTextView.text += traderInformationString
    }
}

// MARK: ARSessionDelegate

extension MainARViewController: ARSessionDelegate {
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        self.trackingStateLabel.text = camera.trackingState.description
        
        switch camera.trackingState {
        case .normal:
            self.sceneView.scene.rootNode.isHidden = false
            self.sceneView.scene.rootNode.childNodes.forEach { $0.isHidden = false }
            
            self.user.fakeLocationService?.isPause = false
            self.trader?.fakeLocationService?.isPause = false
            
            self.trackingStateLabel.alpha = 0.3
            self.trackingStateLabel.isHidden = false
            self.trackingStateLabel.layer.removeAllAnimations()
            UIView.animate(withDuration: 2, animations: {
                self.trackingStateLabel.alpha = 0
            }, completion: { (finished) in
                if finished {
                    self.trackingStateLabel.isHidden = true
                    self.trackingStateLabel.alpha = 0.3
                }
            })
            
            break
        default:
            self.sceneView.scene.rootNode.isHidden = true
            self.user.fakeLocationService?.isPause = true
            self.trader?.fakeLocationService?.isPause = true
            self.trackingStateLabel.isHidden = false
            
            break
        }
    }
}

// MARK: Trackable

extension MainARViewController: Trackable {
    func trackingMyCoordinate(for currentStep: Step) {
        if let instructions = currentStep.instructions, let distance = currentStep.distance {
            self.speakerService.speak(distance: Int(distance), directions: instructions)
        }
        
        self.user.step = currentStep
        self.setUpLogInformationTextView()
        self.delegate?.myStepChanged(to: currentStep)
        
        self.destination.updateNode(animationDuration: self.duration, originCoordinate: self.user.step.coordinate) { [weak self] in
            guard let self = self else { return }
            self.userToDestinationManager.renderingWaypointNodes(rootNode: self.sceneView.scene.rootNode, originCoordinate: self.user.step.coordinate, duration: self.duration)
        }
        
        self.trader?.updateNode(animationDuration: self.duration, originCoordinate: self.user.step.coordinate) { [weak self] in
            guard let self = self else { return }
            self.traderToDestinationManager?.renderingWaypointNodes(rootNode: self.sceneView.scene.rootNode, originCoordinate: self.user.step.coordinate, duration: self.duration)
        }
    }
    
    func trackingTraderCoordinate(for currentStep: Step) {
        self.trader?.step = currentStep
        self.setUpLogInformationTextView()
        self.delegate?.traderStepChanged(to: currentStep)
        self.trader?.updateNode(animationDuration: self.duration, originCoordinate: self.user.step.coordinate, completion: nil)
    }
}
