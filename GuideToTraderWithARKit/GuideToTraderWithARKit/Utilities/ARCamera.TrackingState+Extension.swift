//
//  ARCamera.TrackingState+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 15/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import ARKit

extension ARCamera.TrackingState {
    var description: String {
        switch self {
        case .limited(let reason):
            switch reason {
            case .excessiveMotion:
                return "움직임이 빠른 상태입니다."
            case .initializing:
                return "초기화 작업을 진행 중입니다."
            case .insufficientFeatures:
                return "탐색할 특징이 부족합니다."
            case .relocalizing:
                return "탐색을 재개하고 있습니다."
            }
        case .normal:
            return "최적의 상태로 진입합니다."
        case .notAvailable:
            return "카메라 위치를 추적할 수 없습니다."
        }
    }
}
