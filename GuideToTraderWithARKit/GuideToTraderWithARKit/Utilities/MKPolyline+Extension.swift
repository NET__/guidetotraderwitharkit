//
//  MKPolyline+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 16/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import MapKit
import CoreLocation

extension MKPolyline {
    var coordinates: [CLLocationCoordinate2D] {
        var coords = [CLLocationCoordinate2D](repeating: kCLLocationCoordinate2DInvalid, count: self.pointCount)
        self.getCoordinates(&coords, range: NSRange(location: 0, length: self.pointCount))
        return coords
    }
}
