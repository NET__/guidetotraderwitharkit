//
//  simd_float4x4+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 10/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import GLKit

extension simd_float4x4 {
    func translationMatrix(for translation : vector_float4) -> matrix_float4x4 {
        var matrix = self
        matrix.columns.3 = translation
        return matrix
    }
    
    func rotateAroundY(for degrees: Float) -> matrix_float4x4 {
        var matrix : matrix_float4x4 = self
        
        matrix.columns.0.x = cos(degrees)
        matrix.columns.0.z = -sin(degrees)
        
        matrix.columns.2.x = sin(degrees)
        matrix.columns.2.z = cos(degrees)
        return matrix.inverse
    }
}
