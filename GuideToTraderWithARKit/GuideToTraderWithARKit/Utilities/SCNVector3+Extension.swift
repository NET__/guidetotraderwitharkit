//
//  SCNVector3+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 10/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import ARKit

extension SCNVector3 {
    static func positionFrom(_ transform: matrix_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
    }
}
