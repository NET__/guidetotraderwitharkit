//
//  AnnotationType.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 23/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

enum AnnotationType {
    case me, trader, destination
}
