//
//  CMMotionManager+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 28/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreMotion

extension CMMotionManager {
    static let shared: CMMotionManager = CMMotionManager()
    
    func startQueuedUpdates(using: @escaping (Double) -> (Bool)) {
        if self.isDeviceMotionAvailable, !self.isDeviceMotionActive  {
            self.deviceMotionUpdateInterval = 0.5
            self.startDeviceMotionUpdates(to: OperationQueue.current!, withHandler: { (data, error) in
                                            if let validData = data {
                                                let angle = validData.attitude.pitch.toDegree()
                                                if using(angle) == true {
                                                    self.stopDeviceMotionUpdates()
                                                }
                                            }
            })
        }
    }
}
