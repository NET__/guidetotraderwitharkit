//
//  FakeLocationService.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 10/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreLocation

protocol FakeLocationServiceDelegate: class {
    func trackingLocation(for currentLocation: Step)
}

class FakeLocationService: NSObject {
    weak var delegate: FakeLocationServiceDelegate?
    
    private var timer: Timer?
    private var counter: Int = -1
    
    private let steps: [Step]
    private let periodBetween: Double
    
    var isPause: Bool = true {
        didSet {
            self.isPause ? self.timer?.invalidate() : self.startTimer()
        }
    }
    
    init(steps: [Step], periodBetween: Double, initialCounter: Int? = nil) {
        self.steps = steps
        self.periodBetween = periodBetween
        if let initialCounter = initialCounter {
            self.counter = initialCounter
        }
    }
    
    func setCounter(with step: Step) {
        let firstIndex = self.steps.firstIndex {
            $0.coordinate == step.coordinate
        }        
        guard let counter = firstIndex else { return }
        self.counter = counter
    }
    
    func startTimer() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: self.periodBetween, target: self, selector: #selector(timerHandler(timer:)), userInfo: nil, repeats: true)
    }
    
    func fireTimer() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(timerHandler(timer:)), userInfo: nil, repeats: false)
    }
    
    @objc func timerHandler(timer: Timer) {
        self.counter += 1
        (self.counter >= self.steps.count) ?
            timer.invalidate() : self.delegate?.trackingLocation(for: self.steps[self.counter])
    }
}
