//
//  NodeType.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 23/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

enum NodeType {
    case me, trader, destination, fromMe, fromTrader
}
