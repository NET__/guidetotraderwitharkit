//
//  SpeakerService.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 30/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import AVFoundation

class SpeakerService: NSObject {
    private let synthesizer = AVSpeechSynthesizer()
    private var utterances: [AVSpeechUtterance] = []

    override init() {
        super.init()
        self.synthesizer.delegate = self

        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
            print("[info] .... audioSession properties set successful")

        } catch {
            print("[error] .... audioSession properties weren't set because of an error.")
        }
    }
    
    func speak(distance: Int, directions: String) {
        let speakText = self.synthesizer.isSpeaking ? "이어서 \(directions) 입니다." : "\(distance)m 앞 \(directions) 입니다."
        let utterance = AVSpeechUtterance(string: speakText)
        utterance.rate = 0.4
        utterance.voice = AVSpeechSynthesisVoice(language: "ko-KR")
        self.utterances.append(utterance)
        if !self.synthesizer.isSpeaking {
            self.synthesizer.speak(self.utterances.removeFirst())
        }
    }
}

extension SpeakerService: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if !self.utterances.isEmpty {
            self.synthesizer.speak(self.utterances.removeFirst())
        }
    }
}
