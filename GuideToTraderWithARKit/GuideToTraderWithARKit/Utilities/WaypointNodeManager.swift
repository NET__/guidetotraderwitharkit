//
//  WaypointNodeManager.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 13/02/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import SceneKit
import CoreLocation

class WaypointNodeManager {
    private let waypointNodes: [WaypointNode]

    private var reusableQueue: [SCNNode] = []    
    private var radius: Double
    private var duration: Double = 0
    private var originCoordinate: CLLocationCoordinate2D!
    
    init(steps: [Step], radius: Double, type: NodeType) {
        var temps: [WaypointNode] = []
        for i in 0 ..< steps.count {
            guard let last = steps.last, steps[i].coordinate != last.coordinate else {
                let node = WaypointNode(step: steps[i], type: type, intermediateCoordinate: nil)
                temps.append(node)
                break
            }
            let intermediateCoordinate = steps[i].coordinate.intermediateCoordinate(to: steps[i + 1].coordinate)
            let node = WaypointNode(step: steps[i], type: type, intermediateCoordinate: intermediateCoordinate)
            temps.append(node)
        }
        self.waypointNodes = temps
        self.radius = radius
    }

    func renderingWaypointNodes(rootNode: SCNNode, originCoordinate: CLLocationCoordinate2D ,duration: Double) {
        self.duration = duration
        self.originCoordinate = originCoordinate        
        for i in 0 ..< self.waypointNodes.count - 1 {
            guard self.isInclude(rootNode: rootNode, findNode: self.waypointNodes[i]) == nil else {
                self.manageIncludedData(manageNode: self.waypointNodes[i])
                continue
            }
            
            guard self.needToRendering(rootNode: rootNode, checkNode: self.waypointNodes[i]) else { continue }
            
            self.manageNotIncludedData(manageNode: self.waypointNodes[i], nextNode: self.waypointNodes[i + 1]) { [weak self] in
                guard let self = self else { return }
                rootNode.addChildNode(self.waypointNodes[i])
            }
        }
    }
    
    private func needToRendering(rootNode: SCNNode, checkNode: WaypointNode) -> Bool {
        guard checkNode.type == .fromTrader else { return true }
        return rootNode.childNodes.filter { child in
            guard let child = child as? WaypointNode else { return false }
            return child.step.coordinate == checkNode.step.coordinate
            }.count > 0 ? false : true
    }
    
    private func manageIncludedData(manageNode: WaypointNode) {
        guard let intermediateCoordinate = manageNode.intermediateCoordinate else { return }
        
        if self.isInRangeOfRadius(from: manageNode.step.coordinate, to: self.originCoordinate, radius: self.radius) {
            manageNode.updateNode(animationDuration: self.duration, originCoordinate: self.originCoordinate, coordinate: intermediateCoordinate) {
                manageNode.isHidden = false
            }
            return
        }
        
        manageNode.removeFromParentNode()
        if let node = manageNode.childNodes.first {
            self.reusableQueue.append(node)
            print("[info] .... push geometry node \(String(describing: node.geometry))")
            manageNode.removeAllChildNodes()
        }
    }
    
    private func manageNotIncludedData(manageNode: WaypointNode, nextNode: WaypointNode, completionHandler: (()->())?) {
        guard let intermediateCoordinate = manageNode.intermediateCoordinate else { return }
        
        if self.isInRangeOfRadius(from: manageNode.step.coordinate, to: originCoordinate, radius: self.radius) {
            self.setWaypointNodeGeometryWithReusableQueue(waypointNode: manageNode)
            manageNode.updateNode(nextCoordinate: nextNode.step.coordinate)
            manageNode.updateNode(animationDuration: self.duration, originCoordinate: self.originCoordinate, coordinate: intermediateCoordinate) {
                manageNode.isHidden = true
                completionHandler?()
            }
        }
    }
    
    private func isInclude(rootNode: SCNNode, findNode: WaypointNode) -> WaypointNode? {
        return rootNode.childNodes.filter { child in child == findNode }.first as? WaypointNode
    }
    
    private func isInRangeOfRadius(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D, radius: Double) -> Bool {
        return to.distance(from: from) <= radius
    }
    
    private func setWaypointNodeGeometryWithReusableQueue(waypointNode: WaypointNode) {
        if let reusableNode = self.dequeueReusableWaypointNode() {
            waypointNode.addChildNode(reusableNode)
            print("[info] .... pop geometry node \(String(describing: reusableNode.geometry))")
            return
        }
        
        switch waypointNode.type {
        case .fromMe:
            waypointNode.addNode(named: "art.scnassets/arrow.scn")
        case .fromTrader:
            waypointNode.addNode(named: "art.scnassets/arrow2.scn")
        default:
            break
        }
        print("[info] .... create geometry node \(String(describing: waypointNode.childNodes.first?.geometry))")
    }
    
    private func dequeueReusableWaypointNode() -> SCNNode? {
        print("[info] .... queue count \(self.reusableQueue.count)")
        return self.reusableQueue.isEmpty ? nil : self.reusableQueue.removeFirst()
    }
}
