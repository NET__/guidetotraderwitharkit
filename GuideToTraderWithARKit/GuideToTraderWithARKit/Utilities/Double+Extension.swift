//
//  Double+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 10/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import Foundation

extension Double {
    func toRadians() -> Double {
        return self * .pi / 180.0
    }
    
    func toDegree() -> Double {
        return self * 180 / .pi
    }
    
    func roundUp(at: Int) -> Double {
        let power = Double(truncating: pow(10, at - 1) as NSNumber)
        return (self * power).rounded() / power
    }
}
