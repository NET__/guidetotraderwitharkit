//
//  SCNNode+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 30/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import SceneKit

extension SCNNode {
    func pivotToCenter() {
        let (min, max) = self.boundingBox
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = min.y + 0.5 * (max.y - min.y)
        let dz = min.z + 0.5 * (max.z - min.z)
        
        self.pivot = SCNMatrix4MakeTranslation(dx,dy, dz)
    }
}
