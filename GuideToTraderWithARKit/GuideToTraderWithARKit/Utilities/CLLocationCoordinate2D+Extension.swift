//
//  CLLocationCoordinate2D+Extension.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 15/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreLocation
import MapKit
import GLKit

extension CLLocationCoordinate2D {
    static func == (_ leftHandSide: CLLocationCoordinate2D, rightHandSide: CLLocationCoordinate2D) -> Bool {
        return leftHandSide.latitude == rightHandSide.latitude && leftHandSide.longitude == rightHandSide.longitude
    }
    
    static func != (_ leftHandSide: CLLocationCoordinate2D, rightHandSide: CLLocationCoordinate2D) -> Bool {
        return leftHandSide.latitude != rightHandSide.latitude || leftHandSide.longitude != rightHandSide.longitude
    }
    
    func getDirections(destinationCoordinate: CLLocationCoordinate2D, request: MKDirections.Request, completion: @escaping ([Step], MKRoute) -> Void) {
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: self))
        request.destination = MKMapItem.init(placemark: MKPlacemark(coordinate: destinationCoordinate))
        request.requestsAlternateRoutes = false
        request.transportType = .walking
        
        let directions = MKDirections(request: request)
        
        directions.calculate { response, error in            
            if error != nil {
                print("[error] .... get directions error")
            } else {
                print("[info] .... get directions successful")

                guard let response = response, let uniqueRoute = response.routes.first else { return }
                
                let coordinates: [CLLocationCoordinate2D] = response.routes.first!.polyline.coordinates
                var steps: [Step] = []
                
                coordinates.forEach {
                    var isTurnPoint: Bool = false
                    for i in 0 ..< uniqueRoute.steps.count - 1 {
                        if $0 == uniqueRoute.steps[i].polyline.coordinate {
                            let step = Step(coordinate: $0, instructions: uniqueRoute.steps[i + 1].instructions, distance: uniqueRoute.steps[i + 1].distance)
                            steps.append(step)
                            isTurnPoint = true
                            break
                        }
                    }

                    if !isTurnPoint {
                        steps.append(Step(coordinate: $0))
                    }
                }
                completion(steps, uniqueRoute)
            }
        }
    }
    
    func distance(from coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        let R = 6371.01
        let lat1 = self.latitude.toRadians()
        let lat2 = coordinate.latitude.toRadians()

        let dLat = (coordinate.latitude - self.latitude).toRadians()
        let dLon = (coordinate.longitude - self.longitude).toRadians()

        let a = pow(sin(dLat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dLon / 2), 2)
        let c = 2 * atan2(sqrt(a), sqrt(1 - a))
        let d = R * c

        return d * 1000.0
    }
    
    func bearing(with coordinate: CLLocationCoordinate2D) -> Double {
        let lat1 = self.latitude.toRadians()
        let lon1 = self.longitude.toRadians()
        
        let lat2 = coordinate.latitude.toRadians()
        let lon2 = coordinate.longitude.toRadians()
        let dLon = lon2 - lon1
        
        let x = sin(dLon) * cos(lat2);
        let y = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        let radiansBearing = atan2(x, y)
        return radiansBearing
    }
    
    func transformMatrix(coordinate: CLLocationCoordinate2D) -> simd_float4x4 {
        let distance = Float(coordinate.distance(from: self))
        let bearing = self.bearing(with: coordinate)
        let position = vector_float4(0.0, -0.5, -distance, 0.0)
        let translationMatrix = matrix_identity_float4x4.translationMatrix(for: position)
        let rotationMatrix = matrix_identity_float4x4.rotateAroundY(for: Float(bearing))
        return simd_mul(rotationMatrix, translationMatrix)
    }
    
    func intermediateCoordinate(to coordinate: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let midLat = (self.latitude + coordinate.latitude) / 2
        let midLon = (self.longitude + coordinate.longitude) / 2
        return CLLocationCoordinate2D(latitude: midLat, longitude: midLon)
    }
}
