//
//  POIAnnotation.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 17/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import MapKit

class POIAnnotation: NSObject, MKAnnotation {
    let type: AnnotationType?
    let title: String?
    let glyphText: String?

    var subtitle: String?
    var fakeLocationService: FakeLocationService?
    weak var trackable: Trackable?
    var coordinate: CLLocationCoordinate2D {
        didSet {
            self.subtitle = "\(oldValue.latitude), \(oldValue.longitude)"
        }
    }
    
    init(coordinate: CLLocationCoordinate2D, title: String? = nil, glyphText: String? = nil, userType: AnnotationType? = nil, fakeLocationService: FakeLocationService? = nil) {
        self.coordinate = coordinate
        self.title = title
        self.fakeLocationService = fakeLocationService
        self.type = userType        
        self.subtitle = "\(coordinate.latitude), \(coordinate.longitude)"
        self.glyphText = glyphText
        super.init()
        
        self.fakeLocationService?.delegate = self
    }
}

extension POIAnnotation: FakeLocationServiceDelegate {
    func trackingLocation(for currentLocation: Step) {
        guard let type = self.type else { return }
        switch type {
        case .me:
            self.trackable?.trackingMyCoordinate(for: currentLocation)
            break
        case .trader:
            self.trackable?.trackingTraderCoordinate(for: currentLocation)
            break
        default:
            break
        }
    }
}
