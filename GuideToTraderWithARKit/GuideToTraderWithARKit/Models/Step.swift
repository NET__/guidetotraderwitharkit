//
//  Waypoint.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 21/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreLocation

struct Step {
    let coordinate: CLLocationCoordinate2D
    let instructions: String?
    let distance: Double?
    
    init(coordinate: CLLocationCoordinate2D, instructions: String? = nil, distance: Double? = nil) {
        self.coordinate = coordinate
        self.instructions = instructions
        self.distance = distance
    }
}
