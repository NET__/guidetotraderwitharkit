//
//  WaypointNode.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 25/01/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreLocation
import ARKit

class WaypointNode: SCNNode, Node {
    let type: NodeType
    let intermediateCoordinate: CLLocationCoordinate2D?
    
    var step: Step
    var anchor: ARAnchor? {
        didSet {
            guard let transform = self.anchor?.transform else { return }
            self.position = SCNVector3.positionFrom(transform)
        }
    }

    init(step: Step, type: NodeType, intermediateCoordinate: CLLocationCoordinate2D? = nil) {
        self.step = step
        self.type = type
        self.intermediateCoordinate = intermediateCoordinate
        
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addNode(named: String) {
        let childNode = SCNScene(named: named)!.rootNode.childNodes.first!.clone()
        self.addChildNode(childNode)
    }
    
    func updateNode(nextCoordinate: CLLocationCoordinate2D) {
        DispatchQueue.global(qos: .userInteractive).async {
            let distance = self.step.coordinate.distance(from: nextCoordinate)
            let bearing = self.step.coordinate.bearing(with: nextCoordinate)

            self.eulerAngles.y = Float(-bearing)
            self.scale = SCNVector3(self.scale.x, self.scale.y, Float(Int(distance)))
            self.childNodes.first?.geometry?.materials.forEach {
                $0.diffuse.contentsTransform = SCNMatrix4MakeScale(1, Float(Int(distance)), 0)
            }
        }
    }
}
