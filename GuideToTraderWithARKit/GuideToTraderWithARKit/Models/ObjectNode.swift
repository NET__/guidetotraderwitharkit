//
//  ObjectNode.swift
//  GuideToTraderWithARKit
//
//  Created by USER on 01/02/2019.
//  Copyright © 2019 USER. All rights reserved.
//

import CoreLocation
import ARKit

class ObjectNode: SCNNode, Node {
    let type: NodeType

    weak var trackable: Trackable?
    
    private let title: String

    private var informationNode: SCNNode?

    var step: Step
    var fakeLocationService: FakeLocationService?
    var isSelected: Bool = false
    var anchor: ARAnchor? {
        didSet {
            guard let transform = self.anchor?.transform else { return }
            self.position = SCNVector3.positionFrom(transform)
        }
    }
    
    init(title: String, step: Step, type: NodeType) {
        self.title = title
        self.step = step
        self.type = type
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateNode(animationDuration: Double, originCoordinate: CLLocationCoordinate2D, completion: (()->())?) {
        DispatchQueue.global(qos: .userInteractive).async {
            let translation = originCoordinate.transformMatrix(coordinate: self.step.coordinate)
            let anchor: ARAnchor = ARAnchor(transform: translation)

            SCNTransaction.begin()
            SCNTransaction.animationDuration = animationDuration
            self.anchor = anchor
            
            if let informationNode = self.informationNode {
                informationNode.position = SCNVector3Make(self.position.x, self.position.y + 7.5, self.position.z)
            }
            
            SCNTransaction.commit()
        }
        completion?()
    }
    
    func addInformation() {
        let title = SCNText(string: self.title, extrusionDepth: 0.5)
        title.font = UIFont.systemFont(ofSize: 2)
        title.flatness = 0.01
        title.firstMaterial?.diffuse.contents = UIColor.black
        self.informationNode = SCNNode(geometry: title)
        guard let informationNode = self.informationNode else { return }
        
        let (min, max) = informationNode.boundingBox
        let dx = min.x + 0.5 * (max.x - min.x)
        let dy = max.y
        let dz = min.z + 0.5 * (max.z - min.z)
        
        informationNode.pivot = SCNMatrix4MakeTranslation(dx, dy, dz)
        informationNode.position = SCNVector3Make(self.position.x, self.position.y + 7.5, self.position.z)
        
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        informationNode.constraints = [billboardConstraint]
        
        self.addChildNode(informationNode)
    }
    
    func removeInformation() {
        self.informationNode?.removeFromParentNode()
        self.informationNode = nil
    }
    
    func addNode(named: String) {
        let tempScene = SCNScene(named: named)!
        let root = tempScene.rootNode.clone()
        root.pivotToCenter()
        
        let sphere: SCNSphere = SCNSphere(radius: CGFloat(root.boundingSphere.radius))
        sphere.firstMaterial?.transparency = 0.0
        
        self.geometry = sphere
        self.position = root.boundingSphere.center
        
        self.addChildNode(root)
    }
}

extension ObjectNode: FakeLocationServiceDelegate {
    func trackingLocation(for currentLocation: Step) {
        switch self.type {
        case .me:
            self.trackable?.trackingMyCoordinate(for: currentLocation)
            break
        case .trader:
            self.trackable?.trackingTraderCoordinate(for: currentLocation)
            break
        default:
            break
        }
    }
}
